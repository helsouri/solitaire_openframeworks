#include "ofApp.h"

float cardWidth = 0, cardHeight = 0;

//--------------------------------------------------------------
void ofApp::setup(){

    // discardHand.setupRel(currHand.returnPos(),backImage.getWidth(), backImage.getHeight());

    setupCurrHand();

    setupTableau();

    putCardsInTabSpots();

}

//--------------------------------------------------------------
void ofApp::update(){
}

//--------------------------------------------------------------
void ofApp::draw(){

    currHand.draw();

    discardHand.draw();

    for(auto thisSpot : tabSpots)
    {
        thisSpot.draw();
    }

}


// A function to calculate if a mouse is within a rectangle
// this will serve to detect if mouse is within a card bounding box
// Quick mafs
bool ofApp::mouseInRect(ofRectangle thisRect)
{
    return ( mouseX >= thisRect.position.x && 
             mouseY >= thisRect.position.y &&
             mouseX <= thisRect.position.x + thisRect.width &&
             mouseY <= thisRect.position.y + thisRect.height);
}


// A function to calculate if a mouse is within a rectangle
// using a position and the default card width and height
bool ofApp::mouseInRect(ofPoint thisPoint)
{
    return ( mouseX>= thisPoint.x && 
        mouseY >= thisPoint.y &&
        mouseX <= thisPoint.x + cardWidth &&
        mouseY <= thisPoint.y + cardHeight);
}

void ofApp::setupCurrHand()
{

    char currSuitType = '-';

    int currCardValue = 0;
    

    for (int i=0;i<4;i++)
    {
        card tempCard;
        for(int j=1;j<=13;j++)
        {        
            string imagePath = "images/";
            currCardValue = j;
            switch(i)
            {
                case 0:
                    imagePath+="clubs/c";
                    currSuitType = 'c';
                    break;
                case 1:
                    imagePath+="diamonds/d";
                    currSuitType = 'd';
                    break;
                case 2:
                    imagePath+="hearts/h";
                    currSuitType = 'h';
                    break;
                case 3:
                    imagePath+="spades/s";
                    currSuitType = 's';
                    break;
                default:
                    break;

            }

            if(j<=9){
                imagePath+='0' + to_string(j)+".png";

            }
            else{
                imagePath+=to_string(j)+".png";
            }

            tempCard.initCard(currSuitType,currCardValue, imagePath);
            cardWidth = tempCard.getCardWidth();
            cardHeight = tempCard.getCardHeight();

            currHand.setBasePos(ofPoint(ofGetWindowWidth(), 0) -  ofPoint(cardWidth, 0) - ofPoint(15,0));
            
            currHand.assignCard(tempCard);
        }
    }

    
    currHand.setupBorder(cardWidth, cardHeight);

    currHand.shuffleStack();

    discardHand.setBasePos(currHand.getBasePos() - ofPoint(cardWidth,0) - ofPoint(15,0));
    discardHand.setupBorder(cardWidth, cardHeight);
}

void ofApp::setupTableau()
{
    for(int i=0;i<7;i++)
    {
        tableau tempSpot;

        tempSpot.setupSpotIndex(i);

        tempSpot.setBasePos( ofPoint(i*cardWidth, cardHeight*3) + ofPoint((i+1)*50,0) );

        tabSpots.push_back(tempSpot);
    }
}

void ofApp::putCardsInTabSpots()
{
    /*
        When dealing cards to tableau each stack gets an amound of cards equal to it's spot index
        The stack at spot index 7 gets 7 cards such that 6 cards are facedown and the top most is face up
        The stack at spot index 6 gets 6 cards such that 6 cards are face down and the top most is face up
        However to make the distribution more random we deal cards one stack at a time
    */

    for (int i=0;i<tabSpots.size();i++)
    {
        // int thisSpotIndex = tabSpots.at(i).getSpotIndex();




        for(int j=0;j<tabSpots.at(i).getSpotIndex();j++)
        {
            card tempCard = currHand.cardFromBack();
            currHand.popCard();


            tempCard.setCardPos( tabSpots.at(i).getBasePos() + ofPoint(0, j*20) );

            tabSpots.at(i).assignCard(tempCard);
            
            // tabSpots.at(i).assignCardWithOffset(tempCard, ofPoint(0, 10*j));
        }
    }

    // ofLog()<<"There are " << currHand.getStackSize() << " cards remaining in currHand";
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

    if(!currHand.isEmpty())
    {
        if(button==0 && mouseInRect(currHand.getBasePos()))
        {
            discardHand.assignCard(currHand.cardFromBack());
            currHand.popCard();
        }
    }
    else
    {
        if(button==0 && mouseInRect(currHand.getBasePos()))
        {
            for(auto thisCard : discardHand.getEntireHand())
            {
                thisCard.setCardPos(currHand.getBasePos());
                currHand.assignCard(thisCard);
            }

            discardHand.clearHand();

        }
    }
    
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
