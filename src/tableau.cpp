#include "tableau.h"

tableau::tableau()
{
    spotIndex = -1;
    basePos = ofPoint();
}

void tableau::setupSpotIndex(int thisSpotIndex)
{
    spotIndex = 7 - thisSpotIndex;
}

int tableau::getSpotIndex()
{
    return spotIndex;
}

void tableau::assignCard(card thisCard)
{
    currStack.push_back(thisCard);
}

ofRectangle tableau::getTopCardBoundingBox()
{
    return currStack.back().getCardBoundingBox();
}

void tableau::printTopCardInfo()
{
    currStack.back().printCardInfo();
}

void tableau::printCards()
{
    if(!currStack.empty())
    {
        ofLog()<<endl<<"Spot with index: " << spotIndex <<" has the following cards";
        for(auto thisCard : currStack)
        {
            ofLog()<<thisCard.returnCard();
        }
    }
    else{
        ofLog()<<"empty";
    }

}

void tableau::draw()
{
    
    if(!currStack.empty())
    {
        for(int i = 0 ; i< currStack.size();i++)
        {
            if(i==currStack.size()-1)
            {

                currStack.at(i).setFaceUp(true);
            }
            currStack.at(i).draw();
        }
    }
        
}

void tableau::setBasePos(ofPoint thisPos)
{
    basePos.set(thisPos);
}

ofPoint tableau::getBasePos()
{
    return basePos;
}

tableau::~tableau()
{

}