#ifndef HAND_H
#define HAND_H

#include "ofMain.h"
#include "card.h"

#pragma once

class hand
{
public:
    hand();

    void draw();

    void assignCard(card thisCard);

    void shuffleStack();

    card cardFromBack();
    void popCard();

    void setBasePos(ofPoint thisPos);
    ofPoint getBasePos();

    void setupBorder(float someWidth, float someHeight);

    int getStackSize();

    vector<card> getEntireHand();
    void clearHand();

    bool isEmpty();

    void printCards();
    
    ~hand();

private:
    vector<card> currStack;
    ofPoint handBasePos;
    ofPath handBorder;

};

#endif