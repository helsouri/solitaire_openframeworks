#ifndef TABLEAU_H
#define TABLEAU_H

#include "ofMain.h"
#include "card.h"

#pragma once

class tableau
{
public:
    tableau();

    void setupSpotIndex(int thisSpotIndex);

    int getSpotIndex();

    void assignCard(card thisCard);

    ofRectangle getTopCardBoundingBox();

    void printTopCardInfo();

    void printCards();

    void draw();

    void setBasePos(ofPoint thisPos);
    ofPoint getBasePos();

    ~tableau();

private:
    int spotIndex;
    vector<card> currStack;
    ofPoint basePos;

};

#endif