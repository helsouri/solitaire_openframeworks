#ifndef CARD_H
#define CARD_H


#include "ofMain.h"
#include <string>

#pragma once

class card
{
public:
    card();
    ~card();

    // Only initialize card type, value, and face path
    void initCard(char thisSuitType, int thisCardValue, string thisFacePath);

    // Set card position
    void setCardPos(ofPoint thisPos);

    string returnCard();

    void setFaceUp(bool thisState);
    
    void draw ();

    float getCardWidth();
    float getCardHeight();

    void printCardInfo();

    ofRectangle getCardBoundingBox();

private:
    char suiteType;
    int cardValue;
    ofImage* cardImage;
    string faceImagePath;
    ofImage backImage;
    ofPoint cardPos;
    ofRectangle cardBoundingBox;
    bool isFaceUp;


};

#endif