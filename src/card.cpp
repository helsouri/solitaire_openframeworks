#include "card.h"

card::card()
{
    suiteType = '-';
    cardValue = 0;
    cardImage = new ofImage();
    cardBoundingBox = ofRectangle();
    faceImagePath = "";

    backImage.load("images/cardBack.png");
    isFaceUp = false;
}

card::~card()
{
    
}

void card::initCard(char thisSuitType, int thisCardValue, string thisFacePath)
{
    suiteType = thisSuitType;

    cardValue = thisCardValue;

    faceImagePath = thisFacePath;
}

void card::setCardPos(ofPoint thisPos)
{
    cardPos.set(thisPos);

    cardBoundingBox.set(cardPos, backImage.getWidth(), backImage.getHeight());

}

string card::returnCard()
{
    if(cardValue<=9){
        return suiteType + to_string(cardValue).insert(0,"0");
    }
    else{
        return suiteType + to_string(cardValue);
    }
}

void card::setFaceUp(bool thisState)
{
    isFaceUp = thisState;
    
}

void card::draw()
{
    if(isFaceUp)
    {
        cardImage->load(faceImagePath);
    }
    else
    {
        cardImage = &backImage;
    }
    cardImage->draw(cardPos);
}

float card::getCardWidth()
{
    return backImage.getWidth();
}

float card::getCardHeight()
{
    return backImage.getHeight();
}

void card::printCardInfo()
{
    ofLog()<<"This card has suit "<<suiteType<< " and value " << cardValue;
}

ofRectangle card::getCardBoundingBox()
{
    return cardBoundingBox;
}
