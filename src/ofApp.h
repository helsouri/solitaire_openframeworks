#pragma once

#include "ofMain.h"
#include "card.h"
#include "hand.h"
#include "tableau.h"

#include <iostream>
#include <string>
#include <vector>


extern float cardWidth , cardHeight;

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		vector<tableau> tabSpots;

		char suits[4] = {'c','d','h','s'};
		
		hand currHand;
		hand discardHand;

		// A function to detect if mouse in a rectangle
		// this can be used to see if mouse is over a certain card
		bool mouseInRect(ofRectangle thisRect);
		
		// An overload of mouse in rect function that only requires a point
		// this will be used in instances where mouse is over empty hand/tabley
		// could even more generally used an currHand and discardHand
		bool mouseInRect(ofPoint thisPoint);

		void setupCurrHand();

		void setupTableau();

		void putCardsInTabSpots();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
};
