#include "hand.h"

hand::hand()
{
    handBasePos = ofPoint();
    handBorder = ofPath();
}

void hand::draw()
{
    if(!currStack.empty())
    {
        for(int i = 0 ; i< currStack.size();i++)
        {
            if(i==currStack.size()-1)
            {

                currStack.at(i).setFaceUp(true);
            }
            currStack.at(i).draw();
        }
    }
    else
    {
        handBorder.draw();
    }
}

void hand::assignCard(card thisCard)
{
    thisCard.setCardPos(handBasePos);
    currStack.push_back(thisCard);
}

void hand::shuffleStack()
{
    srand(time(0));

    for (int i=0; i< 52;i++)
    {
        // Random for remaining positions.
        int r = i + (rand() % (52 -i));
 
        swap(currStack.at(i), currStack.at(r));
    }
}

card hand::cardFromBack()
{
    card tempCard = currStack.back();
    return tempCard;
}

void hand::popCard()
{
    currStack.pop_back();
}

void hand::setBasePos(ofPoint thisPos)
{
    handBasePos.set(thisPos);
}

ofPoint hand::getBasePos()
{
    return handBasePos;
}

void hand::setupBorder(float someWidth, float someHeight)
{
    handBorder.setStrokeColor(ofColor::black);
    handBorder.setStrokeWidth(2);

    handBorder.rectangle(handBasePos, someWidth, someHeight);
}

int hand::getStackSize()
{
    return currStack.size();
}

vector<card> hand::getEntireHand()
{
    return currStack;
}

void hand::clearHand()
{
    currStack.clear();
}

bool hand::isEmpty()
{
    return currStack.empty();
}

void hand::printCards()
{
    if(!currStack.empty())
    {
        ofLog()<<"has the following cards";
        for(auto thisCard : currStack)
        {
            ofLog()<<thisCard.returnCard();
        }
        ofLog()<<endl;
    }
    else{
        ofLog()<<"empty"<<endl;
    }
}

hand::~hand()
{  
}