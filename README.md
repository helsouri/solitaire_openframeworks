# Solitaire Openframeworks

A solitaire project using openframeworks

# Setup

- Please see following [link](https://openframeworks.cc/download/) for setup guide

## References

* [Playing Card Images](https://opengameart.org/content/playing-cards-0)
  * Classic cards were used and resized to 10% of original size using [this bulk resize tool](https://imageresizer.com/bulk-resize)
* [openframeworks documentation](https://openframeworks.cc/documentation/)
* [C++ References](https://cplusplus.com/reference/)
